import java.io._

package object logger {
  private var file:PrintWriter = null
  private var opened: Boolean = false
  private var isDebug: Boolean = false

  def openFile(filename: String) = {
    file = new PrintWriter( filename , "UTF-8")
    opened = true;
  }

  def closeFile = {
    if (opened) {
      file.close
      opened = false
    }
  }

  def setDebug(value: Boolean) = isDebug = value

  private def write(s: String, toConsole: Boolean = false) = {
    if (opened) {
      file.println( s )
    }
    if (!opened || toConsole) {
      println( s )
    }
  }

  def info(s: String, toConsole: Boolean = false) = write( s, toConsole )
  def warn(s: String, toConsole: Boolean = false) = write( s"[warn] $s", toConsole )
  def debug(s: String, toConsole: Boolean = false) =
    if (isDebug) write( s"[debug] $s", toConsole )
  def error(s: String, toConsole: Boolean = false) = write( s"[error] $s", toConsole )
}
