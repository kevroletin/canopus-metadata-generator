import java.io._
import scala.language.implicitConversions
import java.io.File
import java.io.OutputStreamWriter
import java.io.PrintWriter
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import org.xml.sax.ErrorHandler
import org.xml.sax.SAXException
import org.xml.sax.SAXParseException
import org.xml.sax.helpers._
import org.xml.sax.InputSource
import org.w3c.dom
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.TransformerException
import javax.xml.transform.TransformerConfigurationException
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys

package object XmlHelpers {
  implicit def strToFile(fileName: String): File = new File(fileName)
  implicit def domDoc2navigator(doc: dom.Document): DomNavigator =
    DomNavigator(doc)
  implicit def domNode2navigator(node: dom.Node): DomNavigator =
    DomNavigator(node)
  implicit def option2navigator(nodeOpt: Option[dom.Node]): DomNavigator =
    DomNavigator(nodeOpt)
  implicit def navigator2option(nav: DomNavigator): Option[dom.Node] =
    nav.nodeOpt

  case class XmlPath(steps: Seq[XmlPathStep] = Seq()) {
    override def toString: String = "_ " + steps.map(_.toString).mkString(" ")

    def apply(doc: dom.Document): Option[dom.Node] = {
      steps.foldLeft(domDoc2navigator(doc)){ (res: DomNavigator, step: XmlPathStep) =>
        res.flatMap(step.apply(_))
      }
    }

    def \(name: String) = XmlPath(steps :+ XmlPathChild(name))

    def \\(name: String) = XmlPath(steps :+ XmlPathAllChilds(name))
  }

  sealed abstract class XmlPathStep {
    def toString: String
    def apply(node: DomNavigator): Option[dom.Node]
  }

  case class XmlPathChild(name: String) extends XmlPathStep {
    override def toString: String = "/ " + name
    override def apply(node: DomNavigator): Option[dom.Node] = node \ name
  }

  case class XmlPathAllChilds(name: String) extends XmlPathStep {
    override def toString: String = "// " + name
    def apply(node: DomNavigator): Option[dom.Node] = node \\ name
  }

  case class DomNavigator(nodeOpt: Option[dom.Node]) {
    private def findAllChilds(name: String): Seq[dom.Node] = nodeOpt.map { node =>
      if (node.getNodeType() != dom.Node.ELEMENT_NODE ||
          !node.hasChildNodes())
      {
        Seq()
      } else {
        val list: dom.NodeList  = node.getChildNodes();
        (0 until list.getLength()).map{ list.item(_) }.filter{ subnode: dom.Node =>
          (subnode.getNodeType() == dom.Node.ELEMENT_NODE
            && subnode.getNodeName().equals(name) )
        }
      }
    } getOrElse Seq()

    private def findFirstChild(name: String): Option[dom.Node] = nodeOpt.flatMap { node =>
      if (node.getNodeType() != dom.Node.ELEMENT_NODE ||
          !node.hasChildNodes())
      {
        None
      } else {
        val list: dom.NodeList  = node.getChildNodes();
        (0 until list.getLength()).map{ list.item(_) }.find{ subnode: dom.Node =>
          (subnode.getNodeType() == dom.Node.ELEMENT_NODE
            && subnode.getNodeName().equals(name) )
        }
      }
    }

    private def findFirstSuccessorHelper(node: dom.Node, name: String): Option[dom.Node] = {
      if (node.getNodeType() != dom.Node.ELEMENT_NODE ||
          !node.hasChildNodes())
      {
        None
      } else if (node.getNodeName().equals(name)) {
        Some(node)
      } else {
        val list: dom.NodeList  = node.getChildNodes();
        val childs = (0 until list.getLength()).map(list.item(_))
        val res = childs.view.map(findFirstSuccessorHelper(_, name)).find(_.nonEmpty)
        res getOrElse None
      }
    }

    private def findFirstSuccessor(name: String): Option[dom.Node] =
      nodeOpt.flatMap(findFirstSuccessorHelper(_, name))

    private def findAllSuccessorsHelper(node: dom.Node, name: String): Seq[dom.Node] = {
      if (node.getNodeType() != dom.Node.ELEMENT_NODE ||
          !node.hasChildNodes())
      {
        Seq()
      } else {
        val res = if (node.getNodeName().equals(name)) Seq(node) else Seq()
        val list: dom.NodeList  = node.getChildNodes();
        val childs = (0 until list.getLength()).map(list.item(_))
        val childRes = childs.flatMap(findAllSuccessorsHelper(_, name))
        res ++ childRes
      }
    }

    private def fundAllSeccessors(childName: String) = nodeOpt.map { node =>
      findAllSuccessorsHelper(node, childName)
    } getOrElse Seq()

    def \(childName: String): DomNavigator = DomNavigator(findFirstChild(childName))

    def \*(childName: String): Seq[dom.Node] = findAllChilds(childName)

    def \\(childName: String): DomNavigator = DomNavigator(findFirstSuccessor(childName))

    def \\*(childName: String): Seq[dom.Node] = fundAllSeccessors(childName)

    def setAttr(name: String, value: String): Boolean = nodeOpt.map { node =>
      val attr = node.getOwnerDocument().createAttribute(name)
      attr.setValue(value)
      val attrContainer = node.getAttributes()
      attrContainer.setNamedItem(attr)
      true
    } getOrElse false

    def replaceBy(rhs: dom.Node): Boolean = nodeOpt.map { node =>
      val doc = node.getOwnerDocument()
      val adoptedRhs = doc.importNode(rhs, true)
      node.getParentNode.replaceChild(adoptedRhs, node)
      true
    } getOrElse false

    def replaceBy(rhsOpt: Option[dom.Node]): Boolean = nodeOpt.map { node =>
      rhsOpt.map { rhs =>
        val doc = node.getOwnerDocument()
        val adoptedRhs = doc.importNode(rhs, true)
        node.getParentNode.replaceChild(adoptedRhs, node)
        true
      } getOrElse false
    } getOrElse false

    def remove: Boolean = nodeOpt.map { node =>
      node.getParentNode.removeChild(node)
      true
    } getOrElse false

    def appendBeforeSelf(rhs: dom.Node): Boolean = nodeOpt.map { node =>
      val doc = node.getOwnerDocument()
      val adoptedRhs = doc.importNode(rhs, true)
      node.getParentNode.insertBefore(adoptedRhs, node)
      true
    } getOrElse false

    def appendAfterSelf(rhs: dom.Node): Boolean = nodeOpt.map { node =>
      val doc = node.getOwnerDocument()
      val adoptedRhs = doc.importNode(rhs, true)
      val next = node.getNextSibling()
      if (next != null) {
        node.getParentNode.insertBefore(adoptedRhs, next)
      } else {
        node.getParentNode.appendChild(adoptedRhs)
      }
      true
    } getOrElse false
  }

  object DomNavigator {
    def apply(doc: dom.Document): DomNavigator = 
      DomNavigator(Some(doc.getDocumentElement()))

    def apply(node: dom.Node): DomNavigator =
      DomNavigator(Some(node))
  }

  def readXmlFromFile(file: File) = {
    val dbf: DocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    val db: DocumentBuilder = dbf.newDocumentBuilder();
    db.parse(file)
  }

  def readXmlFromString(data: String) = {
    val dbf: DocumentBuilderFactory = DocumentBuilderFactory.newInstance();
    val db: DocumentBuilder = dbf.newDocumentBuilder();
    val is: InputSource = new InputSource();
    is.setCharacterStream(new StringReader(data));
    db.parse(is);
  }

  def writeXml(dest: File, document: dom.Document) = {
    logger.debug( s"Write document into $dest" )
    val tFactory:TransformerFactory = TransformerFactory.newInstance();
    val transformer: Transformer = tFactory.newTransformer();
    //transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
    //document.setXmlStandalone(true);

    val source: DOMSource = new DOMSource(document);
    val result: StreamResult = new StreamResult(dest);
    transformer.transform(source, result);
  }
}
