package main

import scala.language.implicitConversions

import java.io._
import scala.io._
import scala.util.{Try, Success, Failure}
import org.w3c.dom
import ImageUtils._

case class JobInput(metadataFile: File, images: Seq[File])

case class Job(input: JobInput,
               template: Template,
               outputDir: File,
               transforms: Seq[Transformation])
{
  import Utils._
  import XmlHelpers._

  private def mkDirs(metadataId: String) = {
    outputDir.mkdirs
    for (d <- Seq("/metadata", "/private", "/public")) {
      new File(outputDir + "/" + metadataId + "/" + d).mkdirs
    }
  }

  private def getIdFromMetadata(doc: dom.Document): Option[String] =
      (doc \\ "gmd:identifier" \\ "gco:CharacterString") map(_.getTextContent)

  def execute() = try {
    val inputMetadata = readXmlFromFile(input.metadataFile)
    val metadataId: String = getIdFromMetadata(inputMetadata).get
    mkDirs(metadataId)

    var resultMetadata = inputMetadata
    for (t <- transforms) {
      try {
        val ok = t(resultMetadata, template.metadata)
        if (!ok) {
          logger.warn(s"${t.name} wasn't applied to ${input.metadataFile}")
        }
      }
      catch {
        case e: Throwable => {
          val msg = s"Error while applying ${t.name} to ${input.metadataFile} -> $e.msg"
          throw new Exception(msg)
        }
      }
    }
    writeXml(outputDir + s"/${metadataId}/metadata/metadata.xml", resultMetadata)

    val resultInfo = template.info
    val files = resultInfo \ "public" \* "file"
    files(0) setAttr("name", s"${metadataId}.png")
    files(1) setAttr("name", s"${metadataId}_s.png")
    writeXml(outputDir + s"/${metadataId}/info.xml", resultInfo)

    // TODO: do we need handle multiple images?
    for {
      node <- resultMetadata \\ "gmd:identifier" \\ "gco:CharacterString"
      img  <- input.images.headOption
    } {
      val id = node.getTextContent
      val bigImage = new File( outputDir + s"/${metadataId}/public/${metadataId}.png")
      val littleImage = new File( outputDir + s"/${metadataId}/public/${metadataId}_s.png")
      ThumbnailScaler(img, bigImage, littleImage, (180, 180))
    }
  }
  catch {
    case e: Throwable => println("[error] " + e.getMessage)
  }
}

case class Template(metadata: dom.Document, info: dom.Document)

object Template {
  import Utils._
  import XmlHelpers._

  def read(templateDir: File): Try[Template] = {
    for {
      template  <- Try( readXmlFromFile( templateDir + "/metadata/metadata.xml" ) )
      info      <- Try( readXmlFromFile( templateDir + "/info.xml" ) )
    } yield {
      Template(template, info)
    }
  }
}

abstract class Transformation {
  def apply(input: dom.Document, template: dom.Document): Boolean
  val name: String
}

case class Config(
  templateDir: String = "template",
  logFile: String = "",
  inDir: String = "In",
  outDir: String = "Out"
)

object TransformationList {
  import Utils._
  import XmlHelpers._

  def copyXmlPart(findNode: DomNavigator => DomNavigator,
    src: dom.Document,
    dest: dom.Document): Boolean = findNode(dest) replaceBy findNode(src)

  object FixLanguage extends Transformation {
    val name = "FixLanguage"

    def apply(input: dom.Document, template: dom.Document): Boolean =
      input \ "gmd:language" \ "gmd:LanguageCode" setAttr ("codeListValue", "rus")
  }

  case class GrabNode(searchFuncs: XmlPath*) extends Transformation {
    val name = "GrabNode (" + searchFuncs(0).toString + ")"

    def apply(input: dom.Document, template: dom.Document): Boolean = {
      val f = searchFuncs(0)
      f(template).map { srcNode =>
        if (f(input) replaceBy srcNode) true
        else {
          searchFuncs.tail.find{ func =>
            func(input).map { node =>
              node.appendAfterSelf(srcNode)
            } getOrElse false
          }.isDefined
        }
      } getOrElse false
    }
  }

  object FixReferenceSystem extends Transformation {
    val name = "FixReferenceSystem"

    private def fixRefText(node: dom.Node): Boolean = {
      val text = node.getTextContent
      if (!text.contains("""PROJCS["WGS 84""")) false
      else {
        val newText = parsers.CoordinateSystemUtil.fix(text)
        newText.map(node.setTextContent(_)).isDefined
      }
    }

    def apply(input: dom.Document, template: dom.Document): Boolean =
      (for {
        node <- (input \ "gmd:referenceSystemInfo")
        a <- (node \\ "gmd:LocalisedCharacterString")
        b <- (node \\ "gco:CharacterString")
      } yield {
        val c = (node \\ "gmd:LocalisedCharacterString").setAttr ("locale", "#ENG")
        c && fixRefText(a) && fixRefText(b)
      }) getOrElse false
  }

  object SwapLanguages {
    def apply(node: dom.Node): Boolean = 
      (for {
        enValue <- node \\ "gco:CharacterString"
        ruValue <- node \\ "gmd:LocalisedCharacterString"
        if ruValue.asInstanceOf[dom.Element].getAttribute("locale") == "#RUS"
      } yield {
        val enText = enValue.getTextContent
        val ruText = ruValue.getTextContent
        ruValue.setTextContent(enText)
        enValue.setTextContent(ruText)
        ruValue setAttr ("locale", "#ENG")
      }) getOrElse false
  }

  case class SwapLanguages(searchFun: dom.Document => Option[dom.Node]) extends Transformation {
    val name = "FixTitle"

    def apply(input: dom.Document, template: dom.Document): Boolean =
      searchFun(input) map { n => SwapLanguages(n) } getOrElse false
  }

  object FixDateStamp extends Transformation {
    val name = "FixDateStamp"

    private def fixNodeDate(node: dom.Node) = {
      val value = node.getTextContent.split("T")(0)
      node.setTextContent(value)
      node.getOwnerDocument().renameNode(node, "", "gco:Date")
      true
    }

    def apply(input: dom.Document, template: dom.Document): Boolean = {
      //val a1 = (input \ "gmd:dateStamp" \ "gco:DateTime") map( fixNodeDate(_) )
      val a2 = (input \ "gmd:identificationInfo" \\ "gco:DateTime") map( fixNodeDate(_) )
      //a1.isDefined && a2.isDefined
      a2.isDefined
    }
  }

  object FixGraphicOverview extends Transformation {
    val name = "FixGraphicOverview"
    
    def imageName(id: String, suffix: String = ""): String = {
      """http://gis.dvo.ru:9190/geonetwork/srv/eng/resources.get""" +
      """?uuid=ReplaceMeWithUUID&fname=""" + id + suffix + ".png"
    }

    private def replaceGraphic(from: dom.Document, to: dom.Document): Seq[dom.Node] =
      (to \\ "gmd:resourceFormat") map { nextNode =>
        (to \\* "gmd:graphicOverview").foreach(_.remove)
        val xs = from \\* "gmd:graphicOverview"
        xs.foreach( x => nextNode.appendBeforeSelf(x) )
        to \\* "gmd:graphicOverview"
      } getOrElse Seq()

    def apply(input: dom.Document, template: dom.Document): Boolean =
      (input \\ "gmd:identifier" \\ "gco:CharacterString") map { idNode =>
        val ys = replaceGraphic(template, input)
        if (ys.length < 2) {
          false
        } else {
          val id = idNode.getTextContent
          val littleImage = imageName(id)
          val bigImage = imageName(id, "_s")
          (ys(1) \\ "gco:CharacterString") map ( _.setTextContent(littleImage) )
          (ys(0) \\ "gco:CharacterString") map ( _.setTextContent(bigImage) )
          true
        }
      } getOrElse false
  }

  object FixDescriptiveKeywords extends Transformation {
    val name = "FixDescriptiveKeywords"

    private def getKeywords(doc: dom.Document): Map[String, dom.Node] = {
      def getGroupType(node: dom.Node): String = {
        (node \ "gmd:MD_Keywords" \ "gmd:type" \ "gmd:MD_KeywordTypeCode") map { x =>
          val res = x.asInstanceOf[dom.Element].getAttribute("codeListValue")
          if ( res != "theme" ) {
            res
          } else {
            val inspire = (node \\ "gmd:code" \\ "gco:CharacterString") map { y =>
              y.getTextContent == "geonetwork.thesaurus.external.theme.inspire-theme"
            } getOrElse false
            if ( inspire ) "inspire" else "theme"
          }
        } getOrElse "#notDefined"
      }
      val keywords = doc \\ "gmd:MD_DataIdentification" \* "gmd:descriptiveKeywords"
      keywords.map(x => (getGroupType(x),x)).toMap
    }

    def apply(input: dom.Document, template: dom.Document): Boolean = {
      val xs = getKeywords(input)
      val ys = getKeywords(template)
      val swapRes = (xs("place") \ "gmd:MD_Keywords" \* "gmd:keyword") map SwapLanguages.apply
      (  xs("theme").replaceBy(ys("theme"))
      && xs("inspire").remove
      && swapRes.forall(t => t) )
    }
  }

  object FixTopicCategory extends Transformation {
    val name = "FixTopicCategory"

    def apply(input: dom.Document, template: dom.Document): Boolean = {
      val xs = input \\ "gmd:MD_DataIdentification" \* "gmd:topicCategory"
      val ys = template \\ "gmd:MD_DataIdentification" \* "gmd:topicCategory"
      if (xs.length == 0) {
        false
      } else {
        for (y <- ys) { xs(0) appendBeforeSelf y }
        for (x <- xs) { x.remove }
        true
      }
    }
  }

  object RemoveTemporalElement extends Transformation {
    val name = "RemoveTemporalElement"

    def apply(input: dom.Document, template: dom.Document): Boolean =
      (input \\ "gmd:temporalElement").remove
  }

  object FixDimentions extends Transformation {
    val name = "FixDimentions"

    def apply(input: dom.Document, template: dom.Document): Boolean = {
      val res = for {
        x <- input \\ "gmd:MD_ImageDescription" \* "gmd:dimension"
        y <- (x \\* "gco:aName") ++ (x \\* "gmd:descriptor")
      } yield {
        SwapLanguages(y)
      }
      res.forall{t=>t}
    }
  }

  val list = Seq( FixLanguage
                , GrabNode(XmlPath() \ "gmd:hierarchyLevelName")
                , GrabNode(XmlPath() \ "gmd:contact")
                , GrabNode(XmlPath() \ "gmd:locale" )
                , FixReferenceSystem
                , SwapLanguages(_ \ "gmd:identificationInfo" \\ "gmd:title")
                , SwapLanguages(_ \ "gmd:identificationInfo" \\ "gmd:alternateTitle")
                , FixDateStamp
                , SwapLanguages(_ \ "gmd:identificationInfo" \\ "gmd:identifier")
                , GrabNode(XmlPath() \\ "gmd:citedResponsibleParty")
                , GrabNode(XmlPath() \\ "gmd:abstract")
                , GrabNode(XmlPath() \\ "gmd:purpose")
                , GrabNode(XmlPath() \\ "gmd:credit")
                , GrabNode(XmlPath() \\ "gmd:status")
                , GrabNode(XmlPath() \\ "gmd:pointOfContact")
                , FixGraphicOverview
                , GrabNode(XmlPath() \\ "gmd:resourceFormat")
                , FixDescriptiveKeywords
                , GrabNode(XmlPath() \\ "gmd:resourceConstraints")
                , GrabNode(XmlPath() \ "gmd:identificationInfo" \\ "gmd:language")
                , GrabNode(XmlPath() \ "gmd:identificationInfo" \\ "gmd:characterSet",
                           XmlPath() \ "gmd:identificationInfo" \\ "gmd:language")
                , FixTopicCategory
                , RemoveTemporalElement
                , GrabNode(XmlPath() \\ "gmd:attributeDescription")
                , FixDimentions
                , SwapLanguages(_ \\ "gmd:processingLevelCode")
                , GrabNode(XmlPath() \\ "gmd:distributionInfo")
                , GrabNode(XmlPath() \\ "gmd:dataQualityInfo")
                )
}

object MetadataExtractor {
  import Utils._
  import XmlHelpers._

  def findJobInputs(dir: File): Seq[JobInput] = {
    val subDirs = dir.listFiles.filter(_.isDirectory)
    val xml     = dir.listFiles.filter(isFileWithExt(_, "xml"))
    val jpg     = dir.listFiles.filter(isFileWithExt(_, "jpg"))
    val jobs = for (x <- xml) yield {
      val pics = fileNameWithoutExt(x.getName).map{ name =>
        jpg.filter(_.getName.startsWith(name))
      }.getOrElse(Array())
      JobInput(x, pics)
    }
    val subJobInputs = subDirs.flatMap(findJobInputs _)
    jobs ++ subJobInputs
  }

  def buildJobs(inDir: File, commonOutDir: File, template: Template): Seq[Job] = {
    findJobInputs(inDir).map { jobInput =>
      Job(jobInput, template, commonOutDir, TransformationList.list)
    }
  }

  def go(config: Config) = {
    val res = for {
      _ <- ensureDirExists(config.inDir, false)
      _ <- ensureDirExists(config.templateDir, false)
      _ <- ensureDirExists(config.outDir, true)
      template <- { logger.info( s"reading template from ${config.templateDir}" )
                    Template.read(config.templateDir) }
    } yield {
      val jobs = buildJobs(config.inDir, config.outDir, template)
      val len = jobs.length
      (jobs.zipWithIndex).foreach{ case (j, i) =>
        logger.info( s"[$i/$len] -> ${j.input.metadataFile}" )
        j.execute()
      }
    }
    if (res.isFailure) println(res.failed.get)
  }

}

object Main {

  val parser = new scopt.OptionParser[Config]("scopt") {
    opt[String]("template") action { (x, c) =>
      c.copy(templateDir = x) } text("directory with template")
    opt[String]("log") action { (x, c) =>
      c.copy(logFile = x) } text("log file")
    opt[String]("in-dir") action { (x, c) =>
      c.copy(inDir = x) } text("input directory")
    opt[String]("out-dir") action { (x, c) =>
      c.copy(outDir = x) } text("output directory")
    help("help") text("prints this usage text")
  }
  
  def main(args: Array[String]): Unit =
    parser.parse(args, Config()) map { config =>
      if ( !config.logFile.isEmpty ) {
        logger.openFile("log.txt")
      }
      logger.info(config.toString)

      MetadataExtractor.go(config)

      logger.closeFile
    }
}
