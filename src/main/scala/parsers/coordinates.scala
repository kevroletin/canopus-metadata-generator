package parsers

import scala.util.matching._

case class Node(var name: String, childs: Seq[Node]) {
  def prettyPrint(offset: Integer = 0): String = {
    val offsetStr = (1 to offset).map(_ => " ").mkString
    if (childs.isEmpty) {
      offsetStr + name + "\n"
    } else {
      ( offsetStr + name + " [\n" +
        childs.map(_.prettyPrint(offset + 2)).mkString +
        offsetStr + "]\n" )
    }
  }

  def restoreText: String =
    if (childs.isEmpty) name
    else {
      name + "[" + childs.map(_.restoreText).mkString(",") + "]"
    }

 
  def deepGet(p: Node => Boolean): Option[Node] = {
    childs.view.map{ child =>
      if (p(child)) Some(child)
      else child.deepGet(p)
    }.find(_.isDefined).flatMap(t => t)
  }
 
  def get(p: Node => Boolean): Option[Node] = childs.find(p)

  def get(i: Integer): Option[Node] =
    if (0 <= i && i < childs.length) Some(childs(i))
    else None
 
  def get(name: String): Option[Node] = get(_.name == name)
}

/* 
 * Hand-written monadic parser
 */

abstract class Parser[T] extends Function1[List[Char], Option[(T, List[Char])]] {
  self =>

  def flatMap[N](f: T => Parser[N]): Parser[N] = new Parser[N] {
    def apply(input: List[Char]): Option[(N, List[Char])] = {
      self(input) match {
        case Some((t, rest)) => f(t)(rest)
        case None => None
      }
    }
  }
 
  def map[N](f: T => N): Parser[N] = new Parser[N] {
    def apply(input: List[Char]): Option[(N, List[Char])] = {
      self(input) match {
        case Some((t, rest)) => Some((f(t), rest))
        case None => None
      }
    }
  }
 
  def or(p: Parser[T]): Parser[T] = new Parser[T] {
    def apply(input: List[Char]): Option[(T, List[Char])] = {
      self(input) match {
        case Some((t, rest)) => Some((t, rest))
        case None => p(input)
      }
    } 
  }
}

object Parser {
  def check(p: Char => Boolean): Parser[Char] = new Parser[Char] {
    def apply(input: List[Char]): Option[(Char, List[Char])] = {
      input match {
        case x :: xs if p(x) => Some(x, xs)
        case _ => None
      }
    }
  }

  def char(c: Char): Parser[Char] = check(x => x == c)
 
  def many[M](parser: Parser[M]): Parser[List[M]] = new Parser[List[M]] {
    private def go(res: List[M], input: List[Char]): (List[M], List[Char]) = {
      parser(input) match {
        case Some((m, rest)) => go(res ++ List(m), rest)
        case _ => (res, input)
      }
    }
    def apply(input: List[Char]): Option[(List[M], List[Char])] = {
      Some(go(List(), input)) 
    }
  }
 
  def sepBy[N, M](element: Parser[N], separator: Parser[M]): Parser[List[N]] = {
    val rest = many(
      for {
        _ <- separator
        e <- element
      } yield e
    )
    for {
      x <- element
      xs <- rest
    } yield x :: xs
  }
 
  def alphanum: Parser[Char] = check(x => x.isLetter || x.isDigit)
 
  def space: Parser[Char] = check(_.isSpaceChar)
}

object Grammar {
  import Parser._

  val notControlChar = check { c =>
    !Set('[', ']', ',').contains(c)
  }

  val compoundElement: Parser[Node] = for {
    name <- many(notControlChar)
    _ <- char('[')
    childs <- sepBy(element, char(','))
    _ <- char(']')   
  } yield Node(name.mkString, childs)
 
  val simpleElement: Parser[Node] = for {
    name <- many(notControlChar)
  } yield Node(name.mkString, List())
 
  val element = compoundElement or simpleElement
}

object CoordinateSystemUtil {
  import Parser._

  /* some magic to match against regexp */
  implicit class Regex(sc: StringContext) {
    def r = new util.matching.Regex(sc.parts.mkString, sc.parts.tail.map(_ => "x"): _*)
  }

  def fix(str: String): Option[String] = {
    val parts = str.split("\\s?:\\s?")
    if (parts.isEmpty) None
    else {
      val input = parts.last
      for {
        (nodes, _) <- Grammar.element(input.toList)
        geo  <- nodes.get(0)
        num  <- nodes.get("AUTHORITY").flatMap(_.get(1))
      } yield {
        geo.name match {
          case r".*WGS 84 / UTM zone (\d\d)${zone}N.*" => {
            num.name = num.name.reverse.drop(2).reverse + zone
          }
          case _ =>
        }
        nodes.restoreText
      }
    }
  }
}
