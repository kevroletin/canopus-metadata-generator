import java.io._
import scala.io._
import scala.util.{Try, Success, Failure}

package object Utils {
  def ensureDirExists(dir: String, create: Boolean): Try[Unit] = {
    val file = new File(dir)
    if (file.exists || (create && file.mkdirs)) Success()
    else Try( throw new Exception(s"$dir directory doesn't exists") )
  }

  def isFileWithExt(f: File, ext: String) = f.getName.toLowerCase.endsWith(ext)

  def fileNameWithoutExt(f: String): Option[String] = {
    val fs = f.split('.').toList
    if (fs.size > 1) Some(fs.init.mkString(".")) else None
  }
}
