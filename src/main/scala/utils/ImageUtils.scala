package ImageUtils

import scala.util.{Try, Success, Failure}
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import java.io.IOException
import javax.imageio.ImageIO
import java.awt.AlphaComposite
import java.awt.RenderingHints
import java.io._

object ThumbnailScaler {

  private def fillRemove(image: BufferedImage, x: Int, y: Int) = {
    import scala.collection.mutable.Queue
    val q: Queue[(Int, Int)] = Queue((x, y))
    while (q.nonEmpty) {
      val (x, y) = q.dequeue
      val pix: Int = image.getRGB(x, y)
      if ((pix & 0xffffff) == 0 &&
          (pix & 0xff000000) != 0 )
      {
        image.setRGB(x, y, 0)
        val w = image.getWidth
        if (x + 1 < w ) { q.enqueue((x + 1, y)) }
        val h = image.getHeight
        if (y + 1 < h ) { q.enqueue((x, y + 1)) }
        if (x > 0) { q.enqueue((x - 1, y)) }
        if (y > 0) { q.enqueue((x, y - 1)) }
      }
    }
  }

  private def removeBlackBorders(image: BufferedImage) = {
    val xx = image.getWidth - 1
    val yy = image.getHeight - 1

    fillRemove(image, 0, 0)
    fillRemove(image, 0, yy/2)
    fillRemove(image, 0, yy)

    fillRemove(image, xx/2, 0)
    fillRemove(image, xx/2, yy)

    fillRemove(image, xx, 0)
    fillRemove(image, xx, yy/2)
    fillRemove(image, xx, yy)
  }

  private def downscaleGeometrySafeAr(image: BufferedImage, toW: Int, toH: Int): ((Int, Int), (Int, Int)) = {
    val fromW = image.getWidth
    val fromH = image.getHeight

    val (w, h) =
      if ( fromW*toH > fromH*toW ) {
        (toW, fromH*toW/fromW)
      } else {
        (fromW*toH/fromH, toH)
      }
    val (x, y) = ((toW - w) / 2, (toH - h) / 2)
    ((w, h), (x, y))
  }

  private def resizeImage(image: BufferedImage,
                          bufferSize: (Int, Int),
                          point: (Int, Int),
                          pictureSize: (Int, Int),
                          antialiasing: Boolean): BufferedImage  =
  {
    val resizedImage = new BufferedImage(bufferSize._1, bufferSize._2, BufferedImage.TYPE_INT_ARGB)
    val g: Graphics2D = resizedImage.createGraphics()

    g.setComposite(AlphaComposite.Src)
    if (antialiasing) {
      g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC)
      g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY)
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
    } else {
      g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF)
      g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR)
      g.setRenderingHint(RenderingHints.KEY_RENDERING,RenderingHints.VALUE_RENDER_SPEED)
    }
    g.drawImage(image, point._1, point._2, pictureSize._1, pictureSize._2, null)
    g.dispose();

    resizedImage
  }

  private def createThumbnail(originalImage: BufferedImage,
                              width: Int,
                              height: Int): BufferedImage =
  {
    val ((iw, ih), _) = downscaleGeometrySafeAr(originalImage, width*2, height*2)
    val intermidiate = resizeImage(originalImage, (iw, ih), (0, 0), (iw, ih), false)
    removeBlackBorders(intermidiate)

    val ((w, h), (x, y)) = downscaleGeometrySafeAr(intermidiate, width, height)
    resizeImage(intermidiate, (width, height), (x, y), (w, h), true)
  }

  def apply(inputFile: File,
            bigImage: File,
            littleImage: File,
            size: (Int, Int)) = {
    val img: BufferedImage = ImageIO.read(inputFile)
    val thumbnail = createThumbnail(img, size._1, size._2)
    ImageIO.write(thumbnail, "png", littleImage)
    ImageIO.write(img, "png", bigImage)
  }
}
