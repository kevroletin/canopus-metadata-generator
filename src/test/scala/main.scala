package mainTests

import org.scalatest._
import scala.util.{Try, Success, Failure}

import main._

object TestUtils {
  def ensureException(statement: => Unit) = {
    Try(statement) match {
      case Failure(_) =>
      case Success(_) => assert( false, "statement should throw error" )
    }
  }
}

object TestData {
  val malformedXml = """<?xml version="1.0"?>"""
  val xmlData01 = """<?xml version="1.0"?>
<html>
<head>
  <title>Good title</title>
</head>
<body>
  <ul class="nav">
    <li>item1</li>
    <li>item2</li>
    <li>item3</li>
  </ul>
</body>
</html>
"""
  lazy val xml01 = XmlHelpers.readXmlFromString(TestData.xmlData01)
}

class DomNavigator extends FunSuite {
  import XmlHelpers._

  test("xml parser works") {
    assert( TestData.xml01 != null )
  }

  test("xml parser throws exception") {
    TestUtils.ensureException {
      XmlHelpers.readXmlFromString(TestData.malformedXml)
    }
  }

  test("simple navigation") {
    val x = TestData.xml01
    assert( (x \ "head" \ "title").get.getTextContent() === "Good title" )
  }

  test("simple navigation with xml path") {
    val x = TestData.xml01
    val p = XmlPath() \ "head" \ "title"
    assert( p(x).get.getTextContent() === "Good title" )
  }

  test("deep navigation") {
    val x = TestData.xml01
    assert( (x \\ "title").get.getTextContent() === "Good title" )
  }

  test("deep navigation with xml path") {
    val x = TestData.xml01
    assert( (XmlPath() \\ "title")(x).get.getTextContent() === "Good title" )
  }

  test("traversal") {
    val x = TestData.xml01
    val y = x \ "body" \ "ul"
    assert(y.isDefined)
    val d = y \* "li"
    assert( d.length === 3 )
  }

  test("deep traversal") {
    val x = TestData.xml01
    val d = x \\* "li"
    assert( d.length === 3 )
  }
}
